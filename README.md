## Parazitai ir sezoniškumas

#### Duomenų surinkimas
Duomenys surinkti 2023-06-19 naudojant [Google Trends](https://trends.google.com/trends/). 

Raktažodžiai:
- **Parazitai**: "Ants" (Insects), "Bed bug" (Topic), "Cockroach" (Insects), "Flea" (Insects), "Lice" (Insects), "Mosquito" (Insects), "Tick" (Animal), "Wasp" (Insects);
- **Kontrolė**: "Bees" (Insects);
- **Priežastys**: "Booking.com" (Topic), "Flight" (Topic), "Hotel" (Accommodation type).

Paieškos nustatymai: "Worldwide" (dėl planetos demografijos visuomet dominuoja šiaurinė hemisfera, tad paieškų sezoniškumas yra būdingas virš ekvatoriaus esančioms valstybėms). Nuo 2010-01-01, imtinai. Raktažodžiai įvedami atskirai vienas nnuo kito (tad populiarumo įvertis normalizuojamas kiekvienam raktažodžiui atskirai)

#### Stulpelių prasmės
- **"month"** - mėnuo, kurio metu gyventojai atliko paieškas (datos formatu YYYY-MM-01)
- **"google_score"** - Google naudojamas paieškos populiarumo įvertis (normalizuotas skalei 0-100)
- **"search_term_orig"** - raktažodis (paieškos žodis/terminas/frazė)
- **"search_term_en"** - raktažodis angliškai, bet susvienodinta rašyba (daugiskaita/vienaskaita)
- **"search_term_lt"** - raktažodis lietuviškai

#### Panaudojimas
Šie duomenys yra vizualizuoti ir aprašyti [DuomenuReikalai.lt](https://duomenureikalai.lt/).
